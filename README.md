# Flectra Community / sale-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[sale_comment_template](sale_comment_template/) | 2.0.2.1.0| Comments texts templates on Sale documents
[sale_order_weight](sale_order_weight/) | 2.0.1.0.0| Add products weight in report for sale order
[sale_report_crossed_out_original_price](sale_report_crossed_out_original_price/) | 2.0.1.1.0| Sale report crossed out original price when a discount exists
[sale_order_report_product_image](sale_order_report_product_image/) | 2.0.1.0.0| Show product images on Sale documents
[sale_order_line_position](sale_order_line_position/) | 2.0.1.1.0| Adds position number on sale order line.
[sale_outgoing_product](sale_outgoing_product/) | 2.0.1.0.0| Module that generates a view for outgoing product


